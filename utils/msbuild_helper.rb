#
# Support for building a csproj file
#
module MSBuildHelper

  def self.find_msbuild
    begin
      `msbuild /version`
    rescue
      # IronRuby 1.0 comes here if msbuild not found
      puts "MSBuild was not found. Please add it to the path."
      exit(2)
    end
    'msbuild'
  end

  def self.build(destination, project, flags="")
    Dir.chdir(destination) do
      puts `#{self.find_msbuild} #{flags} #{project}.csproj`
    end
    return File.join(destination, 'bin').gsub('/','\\')
  end

  def self.build_dll(destination, project)
    build(destination, project, "/p:outputtype=library /verbosity:m")
  end
  
end
