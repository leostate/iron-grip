require File.join(File.dirname(__FILE__),'csproj_utils')
require File.join(File.dirname(__FILE__),'msbuild_helper')
require File.join(File.dirname(__FILE__),'compression')

def conjure_dlls config
    config["dlls"].each do |dll_type, dll_config|
        name = dll_config["name"]
        destination = File.join(config["temp_directory"], name)
        FileUtils.rm_rf destination
        Dir.mkdir destination
        puts "Preparing #{dll_type}"
        copy_files dll_config, File.join(destination,name)
        Compress.in_place(File.join(destination,name)) if config["compress"]
        generate_csproject(destination, name, "DLL.csproj")
        generate_assemblyinfo(destination, name)
        MSBuildHelper.build_dll(destination, name)
    end
end

# Generates Properties/AssemblyInfo.cs unless it exists
def generate_assemblyinfo(destination, project_name)
  FileUtils.mkdir(destination) unless File.exist?(destination)
  destdir  = File.join(destination, 'Properties')
  destfile = File.join(destdir, 'AssemblyInfo.cs')
  unless File.exists?(destfile)
    Dir.mkdir destdir unless File.exists? destdir
    source_info = File.join(File.dirname(__FILE__),'..','resources', 'AssemblyInfo.cs')
    FileUtils.cp source_info, destdir, :verbose => true
    assembly_info = File.read(destfile)
    assembly_info.gsub! /PROJECTNAME/, project_name
    assembly_info.gsub! /YEAR/, Date.today.year.to_s
    File.open(destfile, 'wb') {|f| f.write assembly_info}
  end
  return destfile
end

# Generates <project>.cs unless it exists
def generate_program_cs(config)
  FileUtils.mkdir config["output_directory"] unless File.exist? config["output_directory"]
  destdir  = config["output_directory"]
  project_name = File.basename(config["executable_name"], ".exe")
  destfile = File.join(destdir, "#{project_name}.cs")
  source = File.join(File.dirname(__FILE__),'..','resources', 'Program.cs')
  FileUtils.cp source, destfile, :verbose => true
  source_code = File.read(destfile)
  source_code.gsub! /PROJECTNAME/, project_name
  source_code.gsub! /YEAR/, Date.today.year.to_s
  mount_dlls_string = "\n"
  config["dlls"].each do |key, value|
    if value["mount"]
      value["mount"].each do |path|
        mount_dlls_string += "\t\t\t\ter.Mount(\"#{path}\");\n"
      end
    else
      mount_file_name = File.basename(value["name"], ".dll")
      mount_dlls_string += "\t\t\t\ter.Mount(\"#{mount_file_name}\");\n"
    end
  end
  source_code.gsub! 'er.Mount("APP");', "#{mount_dlls_string}"
  source_code.gsub! 'launcher.rb', config["launcher_file"]
  File.open(destfile, 'wb') {|f| f.write source_code}
  return destfile
end
