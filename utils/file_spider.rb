# Generates a path with clean linux style slashes and removes any ending slashes
def clean_path path
	File.expand_path(path).gsub("\\", "/").split('/').join('/')
end

def copy_file_list list, destination
    list.each do |item|
        puts item
        Dir[clean_path(item)].map do |path| 
            FileUtils.cp_r path, destination
        end
    end
end

# Returns the complete listing of paths specified in the config file for
# a particular type(directories or files).
def get_paths config, path, category
    paths = []
    ["directory", "file"].each do |type|
        if config[category] and config[category]["#{type}_paths"]
            paths += config[category]["#{type}_paths"].map do |path_item|
                item = path_item.gsub("\\", "/").split('/').join('/')
                # if we are looking at a a directory, we need to grab everything in it recursively
                path_array = type == "directory" ? [path,item,"**"] : [path,item]
                Dir[path_array.join("/")]
            end
        end
    end
    paths.flatten.uniq
end

# Returns the list of all files to be including according to the specified config file
def find_paths config, path
	get_paths(config, path, "include") - get_paths(config, path, "exclude")
end

# Copy files according to config
def copy_files config, destination

	source = clean_path(config["source"])
	destination = clean_path(destination)
    FileUtils.rm_rf destination
    Dir.mkdir destination

    boot_file = '_boot_.' + config["name"]
    boot_cmd = IO.read(boot_file) if File.exists?(boot_file)
    boot_dir = File.join(destination, 'EmbeddedRuby')
    Dir.mkdir(boot_dir) unless File.exists?(boot_dir)
    File.open(File.join(boot_dir, 'AppBoot.rb'),"w") {|h| h.puts boot_cmd}


    puts "Copying from: " + source + " to " + destination
    
	# find_paths returns all the paths that we want to put into our libs
	find_paths(config, source).uniq.sort.map do |file|
		file = clean_path(file)
		file_name = file.sub(source + "/", "")
		target_file = "#{destination}/#{file_name}"
		if File.directory? file
			FileUtils.mkdir_p target_file unless File.exist? target_file
		else
			FileUtils.cp "#{file}", target_file
		end
	end
end