require 'pp'
require 'fileutils'
require 'YAML'
require 'utils/build_utils'
require 'utils/file_spider'


@config = YAML.load(IO.read('config.yml'))
puts "Generating files to be included"

puts "Deleting old SKLibraries project"
#FileUtils.rm_rf "SafeKick/SKLibraries"

puts "Copying SKLibraries template"
#FileUtils.cp_r "build_templates/SKLibraries", "SafeKick/SKLibraries"

#copy_file_list @config["copy_to_bin"], @config["output_directory"]
FileUtils.mkdir(@config["temp_directory"]) unless File.exist?(@config["temp_directory"])
conjure_dlls(@config)
#generate_program_cs @config

#clone_build_support(destination)
#FileUtils.mkdir(@config["temp_directory"]) unless File.exist?(@config["temp_directory"])
#dest = File.join(@config["temp_directory"], @config["dlls"]["gems"]["name"])
#generate_assemblyinfo dest, @config["dlls"]["gems"]["name"]